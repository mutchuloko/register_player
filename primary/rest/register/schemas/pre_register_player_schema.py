from marshmallow import Schema, fields


class PreRegisterPlayerSchema(Schema):
    class Meta:
        strict = True

    name = fields.String(allow_none=True, attribute="name")
    cpf = fields.String(allow_none=True)
    birthday = fields.Date(allow_none=True)
    email = fields.Email(allow_none=True)
    zip_code = fields.String(allow_none=True, data_key="zipCode")
    country = fields.String(allow_none=True)
    phone = fields.String(allow_none=True)
    password = fields.String(allow_none=True)