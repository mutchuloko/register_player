from falcon import Request, Response
from pinject import inject

from domain.pre_register.port.primary.pre_register_port import PreRegisterPort
from primary.rest.base_controller import BaseController
from primary.rest.register.dto.pre_register_player_dto import PreRegisterPlayerDto
from primary.rest.register.schemas.pre_register_player_schema import PreRegisterPlayerSchema
from primary.restapi import RestApi


class RegisterController(BaseController):
    serializers = {
        "post": PreRegisterPlayerSchema,
        "get": PreRegisterPlayerSchema
    }

    def __init__(self, pre_register_port: PreRegisterPort):
        super().__init__()
        self.__pre_register_port = pre_register_port

    def on_post(self, req: Request, resp: Response):
        serializer = PreRegisterPlayerDto(**req.context['serializer'])

        resp.media = self.__pre_register_port.save(serializer.to_model()).to_output_dict()

    def on_get(self, req: Request, resp: Response, email: str):
        resp.media = self.__pre_register_port.find_by_email(email).to_output_dict()

    def routes(self):
        RestApi().api.add_route("/register", self)
        RestApi().api.add_route("/register/{email}", self)
