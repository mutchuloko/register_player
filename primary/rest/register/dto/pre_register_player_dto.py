from dataclasses import dataclass
from datetime import datetime

from domain.pre_register.model.pre_register_player import PreRegisterPlayer


@dataclass
class PreRegisterPlayerDto:
    name: str = None
    cpf: str = None
    birthday: datetime = None
    email: str = None
    zip_code: str = None
    country: str = None
    phone: str = None
    password: str = None

    def to_model(self) -> PreRegisterPlayer:
        return PreRegisterPlayer(**self.__dict__)

    @property
    def dict(self):
        return self.__dict__


def to_output_dto(pre_register_player: PreRegisterPlayer) -> PreRegisterPlayerDto:
    return PreRegisterPlayerDto(**pre_register_player.__dict__)


def to_output_dict(pre_register_player: PreRegisterPlayer) -> dict:
    return pre_register_player.to_output_dto().dict


PreRegisterPlayer.to_output_dto = to_output_dto
PreRegisterPlayer.to_output_dict = to_output_dict
