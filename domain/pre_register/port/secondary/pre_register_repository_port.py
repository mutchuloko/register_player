from abc import ABC, abstractmethod

from domain.pre_register.model.pre_register_player import PreRegisterPlayer


class PreRegisterRepositoryPort(ABC):
    @abstractmethod
    def save(self, pre_register_player: PreRegisterPlayer) -> PreRegisterPlayer:
        pass
