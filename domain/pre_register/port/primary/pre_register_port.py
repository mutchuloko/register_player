from abc import ABC, abstractmethod

from domain.pre_register.model.pre_register_player import PreRegisterPlayer


class PreRegisterPort(ABC):
    @abstractmethod
    def save(self, pre_register_player: PreRegisterPlayer) -> PreRegisterPlayer:
        pass

    @abstractmethod
    def find_by_email(self, email: str) -> PreRegisterPlayer:
        pass
