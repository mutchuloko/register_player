
from domain.pre_register.model.pre_register_player import PreRegisterPlayer
from domain.pre_register.port.primary.pre_register_port import PreRegisterPort
from domain.pre_register.port.secondary.pre_register_repository_port import PreRegisterRepositoryPort


class PreRegisterService(PreRegisterPort):

    def __init__(self, pre_register_repository_port: PreRegisterRepositoryPort):
        self.__pre_register_repository_port = pre_register_repository_port

    def save(self, pre_register_player: PreRegisterPlayer) -> PreRegisterPlayer:
        return self.__pre_register_repository_port.save(pre_register_player)

    def find_by_email(self, email: str) -> PreRegisterPlayer:
        return PreRegisterPlayer(name="Enrico", email=email)


