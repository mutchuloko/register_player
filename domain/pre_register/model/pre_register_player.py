from dataclasses import dataclass
from datetime import datetime


@dataclass
class PreRegisterPlayer:
    name: str = None
    cpf: str = None
    birthday: datetime = None
    email: str = None
    zip_code: str = None
    country: str = None
    phone: str = None
    password: str = None
