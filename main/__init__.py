from main.container.service.register.pre_register_container import RegisterContainer
from main.middleware.serializer_middleware import SerializerMiddleware
from main.middleware.sql_session_manager import SQLAlchemySessionManager
from primary.rest.health_controller import HealthController
from primary.rest.register.controller.register_controller import RegisterController
from primary.restapi import RestApi

middleware = [SerializerMiddleware(), SQLAlchemySessionManager()]

api = RestApi(middleware=middleware).api

RestApi.init_controller([RegisterContainer()], [RegisterController, HealthController])
