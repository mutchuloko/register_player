from falcon import HTTP_422, Response, Request
from marshmallow import ValidationError
from marshmallow.schema import Schema

from main.error.http_error_handler import HTTPErrorHandler


class SerializerMiddleware:
    def process_resource(self, req: Request, resp: Response, resource, params):
        req_data: dict = req.media or req.params
        try:
            serializer: Schema = resource.serializers[req.method.lower()]()
        except (AttributeError, IndexError, KeyError):
            return
        else:
            try:
                req.context['serializer'] = serializer.load(data=req_data)
            except ValidationError as err:
                raise HTTPErrorHandler(status=HTTP_422, errors=err.messages)
