from domain.pre_register.model.pre_register_player import PreRegisterPlayer
from domain.pre_register.port.secondary.pre_register_repository_port import PreRegisterRepositoryPort
from secondary.sqlalchemy.session import session


class PreRegisterRepositoryAdapter(PreRegisterRepositoryPort):

    def save(self, pre_register_player: PreRegisterPlayer) -> PreRegisterPlayer:
        session.add(pre_register_player.to_dbo())
        return pre_register_player
