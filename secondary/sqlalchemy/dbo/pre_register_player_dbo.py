from sqlalchemy import Column, String, DateTime

from domain.pre_register.model.pre_register_player import PreRegisterPlayer
from secondary.sqlalchemy.dbo.base_dbo import BaseDBO


class PreRegisterPlayerDBO(BaseDBO):
    __tablename__: str = "pre_register_player"

    name = Column(String)
    cpf = Column(String)
    birthday = Column(DateTime(timezone=True))
    email = Column(String)
    zip_code = Column(String)
    country = Column(String)
    phone = Column(String)
    password = Column(String)

    def to_model(self) -> PreRegisterPlayer:
        return PreRegisterPlayer(
            name=self.name,
            cpf=self.name,
            birthday=self.birthday,
            email=self.email,
            zip_code=self.zip_code,
            country=self.country,
            phone=self.phone,
            password=self.password
        )


def to_dbo(pre_register_player: PreRegisterPlayer) -> PreRegisterPlayerDBO:
    return PreRegisterPlayerDBO(
        name=pre_register_player.name,
        cpf=pre_register_player.cpf,
        birthday=pre_register_player.birthday,
        email=pre_register_player.email,
        zip_code=pre_register_player.zip_code,
        country=pre_register_player.country,
        phone=pre_register_player.phone,
        password=pre_register_player.password
    )


PreRegisterPlayer.to_dbo = to_dbo
