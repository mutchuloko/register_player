import uuid

from dynaconf import settings
from sqlalchemy import Column, DateTime, func, String
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.ext.declarative.base import declared_attr


@as_declarative()
class BaseDBO:
    id = Column(String, primary_key=True, default=str(uuid.uuid4()))
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    # @declared_attr
    # def __table_args__(self):
    #     # print(f'''DB_SCHEMA: {settings.DB_SCHEMA}''')
    #     return {"schema": "main"}

    FIELD = {}